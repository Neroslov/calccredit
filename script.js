jQuery(document).ready(function($) {

const procentKr =12/100;
var ArrayPostOplata =new Array(); 
var mounth =0;
var dolg =0;

	$("#start").click(function pain(event) {
		mounth =parseInt($("#maxDate").val(), 10);
		dolg =parseInt($("#fullCoin").val(), 10);
		nullArray(mounth); //обнулить переменные
		update(); //вывести обновленную таблицу
	});


	function nullArray(){ //поиск и обнуление ячеек
		for(var i =1; i<=mounth; i++)
			ArrayPostOplata[i] =0;
	}
	window.addDopPlat =function(valueMounth){ //диалог изменения
		let trableNull =prompt("Укажите сумму досрочного погашния за "+valueMounth+" месяц:", ArrayPostOplata[valueMounth]);

		if(trableNull != null && Number(trableNull)) ArrayPostOplata[valueMounth] = Number(trableNull);
		// alert(ArrayPostOplata[valueMounth]);
		update();
	}
	window.update =function(){
				var dolgInFunc =dolg;
				var x =dolgInFunc*(procentKr/12 +(procentKr/12)/(Math.pow(1+(procentKr/12),mounth)-1));
				var procentInRub =0; //процент в рублях
				var plat =0;
				var outMax =[0,0,0,0];



		$("#information").empty(); //очистить содержимое
		var outInfo ="<table><tr>";
			outInfo +="<th>Порядковый номер месяца</th>";
			outInfo +="<th>Платеж в счет погашения основного долга, руб</th>";
			outInfo +="<th>Проценты по кредиту, руб</th>";
			outInfo +="<th>Общий ежемесячный платеж, руб</th>";
			outInfo +="<th>Остаток осн. долга после совершения текущего платежа, руб</th>";
			outInfo +="<th>Досрочное погашение </th>";
			outInfo +="</tr>";

		for (var i =1; i <=mounth; i++) {



				procentInRub =dolgInFunc *(procentKr/12);
				plat =x-procentInRub;


				if(ArrayPostOplata[i] != 0){
					x -=ArrayPostOplata[i]*(procentKr/12 +(procentKr/12)/(Math.pow(1+(procentKr/12),(mounth-i))-1));
				}

				dolgInFunc -=(plat +ArrayPostOplata[i]);

			outInfo +="<tr>";
			outInfo +="<td>"+i+" мес.</td>"; //номер месяца
			outInfo +="<td>"+plat.toFixed(2)+"</td>"; outMax[0] +=plat;//погашение кредита
			outInfo +="<td>"+procentInRub.toFixed(2)+"</td>";	outMax[1] +=procentInRub;//проценты по кредиту
			outInfo +="<td>"+x.toFixed(2)+"</td>";	outMax[2] +=x;//общий ежемесячный платёж
			outInfo +="<td>"+dolgInFunc.toFixed(2)+"</td>";	//остаток долга 
			outInfo +="<td onclick ='addDopPlat("+i+");'>"+ArrayPostOplata[i]+"</td>";	outMax[3] +=ArrayPostOplata[i];//изменения
			outInfo +="</tr>";
		}


		outInfo +="<tr class ='lastTr'>";
		outInfo +="<td>Всего: </td>";
		outInfo +="<td>"+outMax[0].toFixed(2)+"</td>";
		outInfo +="<td>"+outMax[1].toFixed(2)+"</td>";
		outInfo +="<td>"+outMax[2].toFixed(2)+"</td>";
		outInfo +="<td></td>"; //ячейка остатка долга пуста
		outInfo +="<td>"+outMax[3].toFixed(2)+"</td>";
		outInfo +="</tr>";

		outInfo +="<tr><td colspan='6'> </td></tr><tr class ='lastTr'>";
		outInfo +="<td colspan='3'>Общая сумма выплат с учетом досрочного погашения:</td>";
		outInfo +="<td colspan='3'>"+(outMax[2]+outMax[3]).toFixed(2)+"</td>";
		outInfo+="</tr>";


		$("#information").append(outInfo+"</table>");
	}

});